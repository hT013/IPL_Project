package com.IPL;

import java.io.*;
import java.sql.*;
import java.util.*;

import static com.IPL.Constant.*;

public class Util {

    public void setDatabaseFromFiles(Connection connection, final String QUERY, final String FILE,
                                     final String TABLE, final TreeSet<Integer> integerIndex) {

        try (BufferedReader csvFile = new BufferedReader(new FileReader(FILE))) {
            Statement statement = connection.createStatement();
            statement.executeUpdate(QUERY);
            String data = csvFile.readLine();
            ArrayList<String> rowData = new ArrayList<>(Arrays.asList(data.split(SPLIT_REGEX)));
            int arraySize = rowData.size();
            String insertQuery = INSERT_INTO + TABLE + VALUES;
            insertQuery += INSERT_VALUE.repeat(arraySize - ONE);
            insertQuery += BRACKET_CLOSE;
            PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
            rowData.clear();
            while ((data = csvFile.readLine()) != null) {
                rowData.addAll(Arrays.asList(data.split(SPLIT_REGEX)));
                if (rowData.size() != arraySize) {
                    for (int i = rowData.size(); i < arraySize; i++)
                        rowData.add(EMPTY_STRING);
                }
                for (int index = 0; index != rowData.size(); index++) {
                    if (integerIndex.contains(index)) {
                        preparedStatement.setInt(index + ONE, Integer.parseInt(rowData.get(index)));
                    } else {
                        preparedStatement.setString(index + ONE, rowData.get(index));
                    }
                }
                preparedStatement.executeUpdate();
                rowData.clear();
            }
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }

    }

}
