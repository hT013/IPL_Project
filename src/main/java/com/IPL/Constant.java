package com.IPL;

import java.util.Arrays;
import java.util.TreeSet;

public class Constant {

    static final int ONE = 1;
    static final String YEAR_2015 = "2015";
    static final String YEAR_2016 = "2016";
    static final String MATCHES = "matches";
    static final String DELIVERIES = "deliveries";
    static final String DATABASE_NAME = "myDatabase";
    static final String USER_NAME = "hT";
    static final String USER_PASSWORD = "1895@hT";
    static final String MATCHES_FILE = "matches.csv";
    static final String DELIVERIES_FILE = "deliveries.csv";
    static final String INSERT_INTO = "Insert into ";
    static final String VALUES = " values(?";
    static final String INSERT_VALUE = ",?";
    static final String BRACKET_CLOSE = ")";
    static final String SPLIT_REGEX = ",(?!\\s)";
    static final String EMPTY_STRING = "";
    static final String TABLE_NAME = "tablename";
    static final String DRIVER_PATH = "jdbc:postgresql://localhost:5432/";
    static final String CHECK_TABLE_QUERY = "SELECT * FROM pg_catalog.pg_tables";

    static final String TIMES_TEAM_WIN_MATCH_QUERY = "SELECT winner, count(result) as result " +
            "from matches group by winner having winner <> '' order by result";

    static final String MATCHES_PER_YEAR_QUERY = "SELECT season, count(season) " +
            "from matches group by season order by season";

    static final String EXTRA_RUN_QUERY = "select bowling_team,sum(extra_runs) as ex_runs from matches,deliveries" +
            "  where matches.season=? and matches.id=deliveries.id " +
            "group by bowling_team order by ex_runs";

    static final String ECONOMY_BOWLER_QUERY = "select bowler, sum(total_runs-legbye_runs-bye_runs)::decimal/" +
            "((count(*) filter ( where wide_runs=0 and noball_runs=0))::decimal/6) as economy " +
            "from matches,deliveries where matches.season=? and matches.id=deliveries.id " +
            "group by bowler order by economy asc limit 5";

    static final String CREATE_MATCHES_TABLE_QUERY = "create table matches (id int NOT NULL , season text, city text," +
            " date text,team1 text,team2 text,toss_winner text,toss_decision text, result text," +
            "dl_applied int,winner text,win_by_runs int,win_by_wickets int,player_of_match text," +
            "venue text,umpire1 text,umpire2 text,umpire3 text, PRIMARY KEY(id))";

    static final String CREATE_DELIVERIES_TABLE_QUERY = "create table deliveries(id int,inning int,batting_team text," +
            " bowling_team text,over int,ball int,batsman text,non_striker text, bowler text," +
            "is_super_over int,wide_runs int,bye_runs int,legbye_runs int,noball_runs int," +
            "penalty_runs int,batsman_runs int,extra_runs int,total_runs int," +
            "player_dismissed text,dismissal_kind text,fielder text)";


    static final TreeSet<Integer> INTEGER_INDEX_MATCHES = new TreeSet<>(Arrays.asList(0, 9, 11, 12));
    static final TreeSet<Integer> INTEGER_INDEX_DELIVERIES = new TreeSet<>
            (Arrays.asList(0, 1, 4, 5, 9, 10, 11, 12, 13, 14, 15, 16, 17));

}
