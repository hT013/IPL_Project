package com.IPL;

import java.sql.*;

import static com.IPL.Constant.*;

public class Main {

    public static void main(String[] args) {
        Main object = new Main();
        try (Connection connection = DriverManager.getConnection(DRIVER_PATH + DATABASE_NAME,
                USER_NAME, USER_PASSWORD)) {

            if (object.checkTable(connection, MATCHES)) {
                new Util().setDatabaseFromFiles(connection, CREATE_MATCHES_TABLE_QUERY, MATCHES_FILE, MATCHES, INTEGER_INDEX_MATCHES);
            }
            if (object.checkTable(connection, DELIVERIES)) {
                new Util().setDatabaseFromFiles(connection, CREATE_DELIVERIES_TABLE_QUERY, DELIVERIES_FILE, DELIVERIES, INTEGER_INDEX_DELIVERIES);
            }

            System.out.format("%-30s%s\n", "Year", "Matches_Played");
            new ExecuteQuery().executeQueryMatches(connection, MATCHES_PER_YEAR_QUERY);

            System.out.format("\n\n%-30s%s\n", "Team Name", "No. of Times Team Won");
            new ExecuteQuery().executeQueryMatches(connection, TIMES_TEAM_WIN_MATCH_QUERY);

            System.out.format("\n\n%-30s%s\n", "Team Name", "Run Conceded");
            new ExecuteQuery().executeQueryDeliveries(connection, EXTRA_RUN_QUERY, YEAR_2016);

            System.out.format("\n\n%-30s%s\n", "Bowler Name", "Economy");
            new ExecuteQuery().executeQueryDeliveries(connection, ECONOMY_BOWLER_QUERY, YEAR_2015);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private boolean checkTable(Connection connection, String fileName) throws SQLException {
        boolean flag = true;
        Statement statement = connection.createStatement();
        ResultSet res = statement.executeQuery(CHECK_TABLE_QUERY);
        while (res.next()) {
            if (res.getString(TABLE_NAME).equals(fileName)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

}

