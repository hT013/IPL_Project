package com.IPL;

import java.sql.*;

import static com.IPL.Constant.*;

public class ExecuteQuery {

    private void displayResultOfExtraRun(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            System.out.format("%-30s%d\n", resultSet.getString(1), resultSet.getInt(2));
        }
    }

    private void displayResultOfEconomyBowler(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            System.out.format("%-30s%.2f\n", resultSet.getString(1), resultSet.getDouble(2));
        }
    }

    public void executeQueryMatches(Connection connection, final String query) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                System.out.format("%-30s%d\n", resultSet.getString(1), resultSet.getInt(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void executeQueryDeliveries(Connection connection, final String QUERY, String year) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
            preparedStatement.setString(1, year);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (QUERY.equals(EXTRA_RUN_QUERY)) {
                displayResultOfExtraRun(resultSet);
            } else {
                displayResultOfEconomyBowler(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
